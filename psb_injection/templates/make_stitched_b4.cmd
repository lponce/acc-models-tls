/******************************************************************
 * Ring 4
 ******************************************************************/


use, sequence = psb4;
seqedit, sequence=psb4;
 flatten;
  install, element= bi4.foil, class=marker, at = 0, from = bi4.tstr1l1;  
 flatten;
  cycle, start = bi4.foil;
 flatten;
endedit;

use, sequence = psb4;

seqedit, sequence = psb4;
    remove, element = bi4.tstr1l1;
endedit;
use, sequence = psb4;

exec, assign_KSW4_strength;
exec, assign_BSW4_strength;
exec, assign_BSW4_alignment;


exec, ptc_twiss_macro(2,0,0);
xpsb04=-table(ptc_twiss,BI4.FOIL,X);


if (buncher == kev_100){
    call, file = "bi_repo/ini_cond_100kev.inp";
} elseif (buncher == kev_250){
    call, file = "bi_repo/ini_cond_250kev.inp";
} elseif (buncher == kev_450){
    call, file = "bi_repo/ini_cond_450kev.inp";
};

call, file = "bi_repo/bi_optics_r4.madx";


BI4PSB4: SEQUENCE, refer = entry, L = 157.08+ 48.51299800 ;
bi4_foil, at=0;
psb4, at =48.51299800  ;
endsequence;

use, sequence = bi4psb4;

SELECT, FLAG=ERROR, RANGE="BI4.QFO50";
EALIGN, DY =yr4;
EPRINT;
SELECT, FLAG=ERROR, RANGE="BI4.QDE60";
EALIGN, DY =yr4;
EPRINT;

exec, assign_KSW4_strength;
exec, bi4_macrobi4();
exec, assign_BSW4_strength;
