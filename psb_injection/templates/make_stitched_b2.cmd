/******************************************************************
 * Ring 2
 ******************************************************************/


use, sequence = psb2;
seqedit, sequence=psb2;
 flatten;
  install, element= bi2.foil, class=marker, at = 0, from = bi2.tstr1l1;  
 flatten;
  cycle, start = bi2.foil;
 flatten;
endedit;

use, sequence = psb2;

seqedit, sequence = psb2;
    remove, element = bi2.tstr1l1;
endedit;
use, sequence = psb2;

exec, assign_KSW2_strength;
exec, assign_BSW2_strength;
exec, assign_BSW2_alignment;


exec, ptc_twiss_macro(2,0,0);
xpsb02=-table(ptc_twiss,BI2.FOIL,X);


if (buncher == kev_100){
    call, file = "bi_repo/ini_cond_100kev.inp";
} elseif (buncher == kev_250){
    call, file = "bi_repo/ini_cond_250kev.inp";
} elseif (buncher == kev_450){
    call, file = "bi_repo/ini_cond_450kev.inp";
};

call, file = "bi_repo/bi_optics_r2.madx";

BI2PSB2: SEQUENCE, refer = entry, L = 157.08+48.51299800  ;
bi2_foil, at=0;
psb2, at = 48.51299800 ;
endsequence;

use, sequence = bi2psb2;

value, yr2;

SELECT, FLAG=ERROR, RANGE="BI2.QFO50";
EALIGN, DY =yr2;
EPRINT;
SELECT, FLAG=ERROR, RANGE="BI2.QDE60";
EALIGN, DY =yr2;
EPRINT;

exec, assign_KSW2_strength;
exec, bi2_macrobi2();
exec, assign_BSW2_strength;
