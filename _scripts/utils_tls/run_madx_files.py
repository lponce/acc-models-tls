from pathlib import Path
import os
import shutil
import subprocess
import sys
import zipfile
import click
import in_place
from tqdm import trange
import time


def find_general(path):
    file_structure = os.walk(path)
    gen_files = {}
    for dirname, subdirs, files in file_structure:

        for filename in files:
            if 'general' in filename:
                gen_files[filename] = dirname
    return gen_files
            
            
def is_psb_inj(file_name):
    return 'psb_inj' in file_name


def run_madx(madx: str, file_name: str):
    command = f'{madx} {file_name} > ~/.tmp'
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()
    home = Path.home()
    os.remove(home / '.tmp')


def change_debuncher_settings(file_name: str, new_settings: int):
    with in_place.InPlace(file_name) as f:
        for line in f:
            if 'buncher = kev_' in line:
                line = f'buncher = kev_{new_settings};\n'
                f.write(line)
            else:
                f.write(line)
    

@click.command()
@click.argument('madx_exe', default='madx')
@click.argument('folder', default='all')
def main(madx_exe, folder):
    
    root_dir = Path(__file__).resolve().parents[2]
    
    psb_debun_sett = [450, 250, 100]

    print('settings: ', madx_exe, folder)
    if folder == 'all':
        gen_files = find_general(root_dir)
    else:
        gen_files = find_general(root_dir / folder)
    
    with trange(len(gen_files.keys())) as t:
    
        for general_file in gen_files.keys():
            t.set_description(f'File: {general_file}')
            os.chdir(gen_files[general_file])
            if is_psb_inj(general_file):
                for i in trange(len(psb_debun_sett), desc='Debuncher settings scan'):
                    db_set = psb_debun_sett[i]
                    change_debuncher_settings(general_file, db_set)
                    run_madx(madx_exe, general_file)
            else:
                run_madx(madx_exe, general_file)
            os.chdir(root_dir)
            
            t.update(1)
            
        
if __name__ == "__main__":
    main()
