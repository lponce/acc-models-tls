!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! 11 November 2016 - M.A. Fraser, F. Velotti
!!
!! In this update, we fix a problem in the survey generation caused by the 
!! changes to the extraction settings from Q26 to Q20, which in fact
!! modified the MAD-X geometry of the line. The beam's initial coordinates
!! are now taken as input and matched back onto the correct transfer line axis
!! using correctors. The transfer line geometry is then as installed and
!! recorded in GEODE. The beam coordinates extraction settings (kickers and
!! bumpers) are taken from those stated in CERN-ATS-Note-2012-095 TECH
!! (E. Gianfelice-Wendt) except for the MSE current that are taken from
!! operational experience: kmse418 = 2.0204e-3, mke4 voltage = 61.65 kV; 
!!
!! 
!! The effect of this update on the optics is negligible (seen on the DX, DY)
!! and amounts to a few small angle RBENDs implemented as errors (correctors).
!! 
!! Changes to TI8 following TCDIL installation (new PC in FODO channel): https://edms.cern.ch/ui/file/1458583/1.0/LHC-TCDI-ES-0004-1-0.pdf
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

option, RBARC=FALSE;
option, echo;

title,   "TI8 line: LIU TCDIs for SPS Q20 optics";
/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt40ti8 ti8_repo";

set,    format="18.12f";
option, -echo, warn, info;

/***************************************
* Load model
***************************************/

call,    file = "ti8_repo/ti8.seq";
call,    file = "ti8_repo/ti8_apertures.dbx";
call,    file = "./ti8_liu.str";

option,  echo, warn, info;

beam,    sequence=ti8, particle=proton, pc= 450;
use,     sequence=ti8;

! Errors on b2 and b3 as measured
call, file="ti8_repo/mbi_b3_error.madx";

! Using initial dipoles to correct extraction trajectory caused by changing optics in the SPS Q26 => Q20
call, file="./trajectory_error_q26_to_q20.madx";

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Q20 postion - Q26 position, relative to nominal axis at SPS extraction
!! "TI8$START"
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

x0 = 0.2669019917 - 0.2643833447;
xp0 = 0.01120541487 - 0.01140006945; 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

beta_Q20: beta0,  betx = 27.93108087 , alfx = 0.65054900,
                    bety = 120.05843813, alfy =-2.70513448,
                    dx=  -0.58140484, dpx=  0.01446870;

beta_Q20_meas: beta0,  betx =27.93108087,bety =  120.05843813,
                       alfx =    0.65054900, alfy =-2.70513448,
                       dx=   -0.51953884,dpx=    0.01357203,   
                       dy=0.07474805, dpy= -0.00011898;


select, flag=twiss, column=NAME, KEYWORD, S, L, TILT, KICK, HKICK, VKICK, ANGLE, K0L, K0SL, K1L, K1SL, K2L, K2SL, K3L, K3SL, BETX, BETY, X, PX, Y, PY, DX, DPX, DY, DPY, ALFX, ALFY, MUX, MUY;
set,    format="27.17f";
Twiss,file="./twiss_ti8_nom.tfs",beta0=beta_Q20,x=x0,px=xp0; 

select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey,
          x0=x0.ti8, y0=y0.ti8, z0=z0.ti8,
          theta0=theta0.ti8, phi0=phi0.ti8, psi0=psi0.ti8;

write, table=survey, file="./survey_ti8.tfs";

/***********************************
* Cleaning up
***********************************/
system, "rm ti8_repo";
stop;