/*****************************************************
 * MADX optics file for SFTPRO transfer (MTE beam) to SPS
 * Based on files from E. Benedetto and S. Gilardoni, checked by A. Huschauer
 *
 * F.Velotti
 *****************************************************/
 title, "TT2/TT10/SPS for SFTPRO (MTE) optics. Protons - 14 GeV/c";

 option, echo;
 option, RBARC=FALSE;  ! the length of a rectangular magnet
                       ! is the distance between the polefaces
                       ! and not the arc length

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm tt10sps_sftpro_savedseq.seq";
system, "rm ./jmad/*";

/*******************************************************************************
 * Beam
 *******************************************************************************/
Beam, particle=PROTON, pc=14;
BRHO      := BEAM->PC * 3.3356;

/***************************************
* Load needed repos
***************************************/
system,"[ ! -e sps_repo ] && [ -d ./../../sps_repo ] && ln -nfs ./../../sps_repo sps_repo";
system,"[ ! -e sps_repo ] && [ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo";
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../../ps_extraction/tt2 tt2_repo";
system, "ln -fns ./../../../ps_extraction/tt10 tt10_repo";
system, "ln -fns ./../../../ps_extraction/tt2tt10_sftpro/line tt2tt10_sftpro_repo";

/*****************************************************************************
 * TT2
 *****************************************************************************/
 option, -echo;
 call, file = "./tt2_repo/tt2.ele";
 call, file = "./tt2tt10_sftpro_repo/tt2_mte_2010.str";
 call, file = "./tt2_repo/tt2.seq";
 call, file = "./tt2_repo/tt2.dbx";

 option, echo;


/*******************************************************************************
 * TT10
 *******************************************************************************/

 call, file = "./tt10_repo/tt10.ele";
 call, file = "./tt2tt10_sftpro_repo/tt10_mte_2010.str";
 call, file = "./tt10_repo/tt10.seq";
 call, file = "./tt10_repo/tt10.dbx";


/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/

 tt2tt10: sequence, refer=ENTRY, l = 1164.8409;
  tt2a                  , at =        0;
  tt2b                  , at = 136.3114;
  tt2c                  , at = 249.9449;
  tt10                  , at = 304.6954;
 endsequence;


/*******************************************************************************
 * set initial twiss parameters
 *******************************************************************************/
! Those should be the correct initial conditions as obtained from the PS
!call, file = "./../stitched/tt2_mte_from_stitched.inp";

! Hystorical conditions and kept only until re-matching done
call, file = "./tt2tt10_sftpro_repo/../stitched/tt2_mte_2010.inp";


INITBETA0: BETA0,
  BETX=BETX0,
  ALFX=ALFX0,
  MUX=MUX0,
  BETY=BETY0,
  ALFY=ALFY0,
  MUY=MUY0,
  X=X0,
  PX=PX0,
  Y=Y0,
  PY=PY0,
  T=T0,
  PT=PT0,
  DX=DX0,
  DPX=DPX0,
  DY=DY0,
  DPY=DPY0;


/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence=tt2tt10;

select, flag = twiss, clear;
savebeta, label = start_tt10, place = endtt2c;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt2tt10_sftpro.tfs";

len_tt2 = table(twiss, endtt2c, s);
len_tt10 = table(twiss, ENDVKNV11839, s) - len_tt2;

/*******************************************************************************
 * Load SPS model
 *******************************************************************************/

call, file = "sps_repo/sps.seq";
call, file = "sps_repo/strengths/ft_q26.str";

 ENDTT10 : MARKER;
 ENDVKNV : MARKER;

 SEQEDIT, SEQUENCE=SPS;
    remove, element = bipmv.51694;
    install, element = ENDVKNV, at = -0.925, from = BTV.11860;
 ENDEDIT;

/*******************************************************************************
 * Make SPS twiss file
 * Nominal MSI settings = -0.010225
 *******************************************************************************/

use, sequence = sps;
 ! MSI settings to optimise aperture
 kmsi    = -0.04164051/4;

 ! MKP settings
 mkp_volt = 50.56;
 rebalancing = 0.7;

 Bdl_mkpa_50 = 0.0883 ;
 Bdl_mkpc_50 = 0.0353 ;
 Bdl_mkp_50 = 0.133 ;

 mkpa_kv2rad = (1 / 50) * Bdl_mkpa_50 / BRHO;
 mkpc_kv2rad = (1 / 50) * Bdl_mkpc_50 / BRHO;
 mkpl_kv2rad = rebalancing * (1 / 50) * Bdl_mkpl_50 / BRHO;

use, sequence = sps;
select, flag = twiss, clear;
savebeta, label=end_inj, place = end.10010;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, file="./twiss_sps_sftpro.tfs";

len_sps = table(twiss, end.10010, s);

/*******************************************************************************
 * Reverse SPS for extraction and get (x, px) at injection
 *******************************************************************************/
seqedit, sequence = sps; 
	extract, sequence=sps, from=endvknv, to=end.10010, newname=spsinj;
endedit;

use, sequence = spsinj;

seqedit, sequence=spsinj;
    flatten;
    reflect;
    flatten;
endedit;

! SFTPRO uses only 3 generators
kmkp11955  := 0.0;
kmkpa11931 := mkp_volt * mkpa_kv2rad;
kmkpa11936 := mkp_volt * mkpa_kv2rad;
kmkpc11952 := mkp_volt * mkpc_kv2rad;

end_inj->alfx = -1 * end_inj->alfx;
end_inj->alfy = -1 * end_inj->alfy;
end_inj->px = -1 * end_inj->px;
end_inj->py = -1 * end_inj->py;
end_inj->dpx = -1 * end_inj->dpx;
end_inj->dpy = -1 * end_inj->dpy;

use, sequence = spsinj;
select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=end_inj, file="./twiss_sps_sftpro_inj_rev.tfs";


x_inj  = table(twiss, ENDVKNV, x);
px_inj = -1 * table(twiss, ENDVKNV, px);

value, x_inj, px_inj;

change_ref: MATRIX, L=0,  kick1 = x_inj, kick2 = px_inj, rm26= px_inj/(beam->beta), rm51 = -px_inj/(beam->beta);

/*******************************************************************************
 * build up the complete model => TT2 + TT10 + SPS
 *******************************************************************************/

use, sequence = sps;
! Changing reference at injection
 SEQEDIT, SEQUENCE=sps;
    install, element = change_ref, at = 1e-10, from = ENDVKNV;
    flatten;
 ENDEDIT;

use, sequence = sps;

SEQEDIT, SEQUENCE=SPS;
    flatten ; cycle, start=  ENDVKNV;
ENDEDIT;


tt2tt10sps: sequence, refer=entry, l = 8076.3447;
    tt2tt10, at = 0.0;
    sps, at = 0.0, from = ENDVKNV11839;
endsequence;

/*******************************************************************************
 * Make twiss of TT2 + TT10 + SPS
 *******************************************************************************/

use, sequence = tt2tt10sps;

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0 = initbeta0;
write, table=twiss, file="./twiss_tt2tt10sps_sftpro.tfs";


/*******************************************************************************
 * Make nominal twiss of TT10 + SPS sequence and dump to file the sequence
 *******************************************************************************/

tt10sps: sequence, refer=entry, l = len_tt10 + len_sps;
    tt10                  , at = 0.0;
    sps, at = 0.0, from = ENDVKNV11839;
endsequence;


use, sequence = tt10sps;

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0 = start_tt10;
write, table=twiss, file="./twiss_tt10sps_sftpro_nom.tfs";


/***********************************************
* Save sequence and initial conditions for JMAD
***********************************************/

set, format="10.8f";
option, -warn;
save, sequence = tt10sps, file="./jmad/tt10sps_sftpro_savedseq.seq", beam;
option, warn;

assign, echo="./jmad/tt10sps_sftpro.inp";

betx0 = start_tt10->betx;
bety0 = start_tt10->bety;
alfx0 = start_tt10->alfx;
alfy0 = start_tt10->alfy;

dx0 = start_tt10->dx;
dy0 = start_tt10->dy;

dpx0 = start_tt10->dpx;
dpy0 = start_tt10->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction to TT10 LHC Q20 beam';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;
/************************************
* Cleaning up
************************************/
system, "rm tt2_repo";
system, "rm tt10_repo";
system, "rm -rf sps_repo || rm sps_repo";
system, "rm tt2tt10_sftpro_repo";

stop;

