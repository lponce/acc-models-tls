!------------------------------------------------------------
! ETP LEIR to PS.  Injection into PS
! Sequence file
!
! Based on EP injection line in MAD8 from C. Carli
! Converted to MADX   Jan 2008 O.Berrig
!------------------------------------------------------------

! ETP.BHN10
DEPB1 = 0.047882200952;
IFR = 0.35;

! Optics from 2016 (Alex and Verena)
/* KETPQDN10 = -0.206603416446; */
/* KETPQFN20 =  0.25491648304; */

! Rematching from Reyes and Enrico to match to PS
ketp.qdn10 := -2.50506e-01;
ketp.qfn20 := 2.48753e-01;
/**************************************************
 * Bending magnets
 **************************************************/
LEPB1 = 0.58;
ETP.BHN10: SBEND, L:=LEPB1*DEPB1/(2.0*SIN(DEPB1/2.0))
                , ANGLE:=DEPB1, E1:=DEPB1/2.0, E2:=DEPB1/2.0
                , HGAP:=0.06, FINT:=IFR;

ETP.DVN10: MARKER;


/**************************************************
 * Quadrupole magnets
 **************************************************/
ETP.QDN10: QUADRUPOLE, L=0.656, K1:=KETP.QDN10; ! -0.206603416446;
ETP.QFN20: QUADRUPOLE, L=0.656, K1:=KETP.QFN20; !  0.25491648304;


/**************************************************
 * Monitors
 **************************************************/
ETP.UEHV10: MONITOR;
ETP.MTR10 : MARKER;
ETP.MTV10 : MARKER;


/**************************************************
 * Vacuum pump
 **************************************************/
ETP.VPG10: MARKER;


/**************************************************
 * Vacuum valve
 **************************************************/
ETP.VVS10 : MARKER;


/**************************************************
 * Element, which simulates the stray fields of a PS magnet
 **************************************************/

scale=1;
fringe.field: matrix, type=fringe, 
l=6.4074+0.0926, 
rm11=1.125*scale, 
rm12=6.534*scale, 
rm21=0.0503*scale, 
rm22=1.181*scale, 
rm33=0.878*scale, 
rm34=5.683*scale, 
rm43=-0.049*scale, 
rm44=0.825*scale, 
rm16=0.022*scale, 
rm26=0.008*scale;

ETP : sequence, refer=centre,  l =   18.7016;

  ETP.START   :  marker     , at =    0.00000;
  ETP.QDN10                 , at =    0.71266;
  ETP.BHN10                 , at =    1.90646;
  ETP.VPG10                 , at =    3.06230;
  ETP.UEHV10                , at =    3.54986;
  ETP.QFN20                 , at =    5.82466;
  ETP.MTV10                 , at =    6.79976;
  ETP.DVN10                 , at =    8.66466;
  ETP.MTR10                 , at =    9.89800;
  ETP.VVS10                 , at =    12.2016;
  fringe.field              , at =    15.4516; ! MATRIX simulating the stray fields inside the PS magnet. Made by C.Carli
  ETP.STOP    :  marker     , at =    18.7016;

endsequence;

